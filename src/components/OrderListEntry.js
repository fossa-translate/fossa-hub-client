import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowRight} from '@fortawesome/free-solid-svg-icons';

function OrderListEntry({order, buttons, match}) {
  const {
    orderNumber,
    humanDateTo,
    humanDateFrom,
    languageTo,
    languageFrom,
    name,
    contacts,
    price,
    prepayment,
    express,
  } = order;
  const className = 'order-item' + `${(express && ' order-item-express') || ''}`;
  return (
    <div className={className}>
      <div className="order-title">
        <p className="order-id">{orderNumber}</p>
        <p className="order-name">{name}</p>
      </div>
      <div className="order-description">
        <div className="order-row">
          <div className="order-column">
            <div className="order-entry">
              <div className="order-entry-title">Язык:</div>
              <div>
                {languageTo}
                {' на '}
                {languageFrom && languageFrom.toLowerCase()}
              </div>
            </div>
            <div className="order-entry">
              <div className="order-entry-title">От:</div>
              {humanDateFrom}
            </div>
            <div className="order-entry">
              <div className="order-entry-title">До:</div>
              {humanDateTo}
            </div>
          </div>
          <div className="order-column">
            <div className="order-entry">
              <div className="order-entry-title">Контакты:</div>
              {contacts}
            </div>
            <div className="order-entry">
              <div className="order-entry-title">Цена:</div>
              {price}
            </div>
            <div className="order-entry">
              <div className="order-entry-title">Предоплата:</div>
              {prepayment}
            </div>
          </div>
        </div>
        <div className="order-buttons">
          {buttons.map((Button, i) => {
            return <Button key={i} order={order} match={match}/>;
          })}
        </div>
      </div>
    </div>
  );
}

export default OrderListEntry;
