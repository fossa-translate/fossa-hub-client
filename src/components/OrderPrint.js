import cutLineImage from '../images/print/cut-line.jpg';
import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';
import {getOrder} from '../slices/orderSlice';

function CutLine() {
  return <img src={cutLineImage} alt="cut-line"/>
}

function OrderPrint({match}) {
  const dispatch = useDispatch();
  const dataSelector = (store) => store.order.current;
  const order = useSelector(dataSelector);

  useEffect(() => {
    if (!order) {
      const orderNumber = match.params.id;
      dispatch(getOrder(orderNumber));
    }
  })

  if (!order) {
   return 'Loading...';
  }

  return (
    <div className="print-table">
      <div className="receipt">
        <div className="print-top">
          <div className="order-meta">
            <div>Заказ № <b>{order.orderNumber}</b></div>
            {order.expressCheck && <div className="print-express"><u>Срочный</u></div>}
          </div>
          <div>от {order.humanDateFrom} до {order.humanDateTo}</div>
        </div>
        <div className="print-row">
          <div className="print-leftblock">
            <div>
              <span className="print-label">ФИО клиента:</span>
              <span>{order.name}</span>
            </div>
            <div>
              <span className="print-label">Контакты:</span>
              <span>{order.contacts}</span>
            </div>
            <div>
              <span className="print-label">Языковая пара:</span>
              <span>{order.language}</span>
            </div>
            <div>
              <span className="print-label">Документы:</span>
              <span>{order.listDocs}</span>
            </div>
            <div>
              <span className="print-label">Рекомендации:</span>
              <span>{order.additionalMaterials}</span>
            </div>
            <div>
              <span className="print-label">Откуда вы узнали про нас?</span>
              <span>{order.whereyouknow}</span>
            </div>
          </div>
          <div className="print-rightblock">
            <div className="cost">
              <span className="print-label">Страниц:</span>
              <span className="cost-number">{order.pageCount}</span>
            </div>
            <div className="cost">
              <span className="print-label">Услуги бюро:</span>
              <span className="cost-number">{order.bureauPrice} ₽</span>
            </div>
            <div className="cost">
              <span className="print-label">Нотариус:</span>
              <span className="cost-number">{order.notaryPrice} ₽</span>
            </div>
            <div className="cost">
              <span className="print-label">Предоплата:</span>
              <span className="cost-number">{order.prepayment} ₽</span>
            </div>
            <div className="cost">
              <span className="print-label">Доплата:</span>
              <span className="cost-number">{order.surcharge} ₽</span>
            </div>
            <div className="final-cost">
              <span className="final-cost-label">Итого:</span>
              <span>{order.price} ₽</span>
            </div>
          </div>
        </div>
        <div className="signature-row">
          <div>Подписывая бланк, вы даете согласие на обработку персональных данных</div>
          <div className="person-signature">(подпись клиента)</div>
        </div>
      </div>
      <CutLine/>
      <div className="receipt">
        <div className="print-leftblock">
          <div>Квитанция № {order.orderNumber}</div>
          <div>Выдана (кому): <u>{order.name}</u></div>
          <div>Получено: <u>{order.prepayment} ₽</u>{order.listDocs && `, ${order.listDocs}`}</div>
          <div>Ваш заказ будет готов <u>{order.humanDateTo}</u></div>
        </div>
        <div className="signature-row margin0">
          <div>Доплата <u>{order.surcharge} ₽</u> после получения</div>
          <div className="signature-wrapper">
            <div className="signature-column">
              <div>{order.humanDateFrom}</div>
              <div className="signature">(дата формирования заказа)</div>
            </div>

            <div className="person-signature">(подпись переводчика)</div>
          </div>
        </div>
      </div>
      <CutLine/>
    </div>
  );
}

export default OrderPrint;
