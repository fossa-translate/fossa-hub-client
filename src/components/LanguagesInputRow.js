import languages from '../languages.json';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {useState} from 'react';

function LanguageInput({id, value, title, onChange}) {
  return (
    <div className="input">
      <label htmlFor={id}>{title}</label>
      <select id={id} name={id} value={value} onChange={onChange}>
        {languages.list.map((lang, i) => <option key={i} value={lang}>{lang}</option>)}
      </select>
    </div>
  );
}

function LanguagesInputRow({from, to}) {
  const [languageTo, setLanguageTo] = useState(from);
  const [languageFrom, setLanguageFrom] = useState(to);

  const onClickSwapLanguages = () => {
    const s = languageTo;
    setLanguageTo(languageFrom);
    setLanguageFrom(s);
  };
  const onChangeLanguageTo = ({target}) => setLanguageTo(target.value);
  const onChangeLanguageFrom = ({target}) => setLanguageFrom(target.value);

  return (
    <div className="input-row">
      <LanguageInput value={languageFrom} title='С' id='languageFrom' onChange={onChangeLanguageFrom}/>
      <div className="order-swap-languages" onClick={onClickSwapLanguages}>
        <FontAwesomeIcon icon={faExchangeAlt}/>
      </div>
      <LanguageInput value={languageTo} title='На' id='languageTo' onChange={onChangeLanguageTo}/>
    </div>);
}

export default LanguagesInputRow;
