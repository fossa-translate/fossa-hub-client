import tabs from '../tabs';

function OrderListTabs({tabName, onClick}) {
  return (
    <ul className="orders-menu">
      {
        Object.entries(tabs).map(([key, value]) => {
          const className = 'orders-tab' + `${(tabName === key && ' li-active') || ''}`;
          return (
            <li
              className={className}
              key={key}
              id={key}
              onClick={onClick}
            >
              {value.title}
            </li>
          );
        })
      }
    </ul>
  );
}

export default OrderListTabs;
