import {useSelector} from 'react-redux';
import {
  BrowserRouter, Redirect, Route, Switch,
} from 'react-router-dom';
import LoginPage from '../pages/LoginPage';
import Navigation from './Navigation';
import OrdersPage from '../pages/OrdersPage';
import NewOrderPage from '../pages/NewOrderPage';
import OrderPrint from './OrderPrint';
import EditOrderPage from '../pages/EditOrderPage';
import SettingsPage from '../pages/SettingsPage';
import NotificationPill from "./NotificationPill";
import NotificationsBlock from "./NotificationsBlock";

function App() {
  const dataSelector = (store) => store.user.profile;
  const profile = useSelector(dataSelector);

  if (!profile) {
    return <LoginPage/>;
  }

  return (
    <BrowserRouter>
      <Navigation/>
      <NotificationsBlock/>
      <div className="app-wrapper">
        <Switch>
          <Route path="/" exact>
            <Redirect to="/orders"/>
          </Route>
          <Route path="/orders" exact>
            <Redirect to="/orders/new"/>
          </Route>
          <Route path="/settings" component={SettingsPage}/>

          <Route path="/orders/:tabName" component={OrdersPage}/>
          <Route path="/order/new" component={NewOrderPage}/>
          <Route path="/order/edit" exact>
            <Redirect to="/order/new"/>
          </Route>
          <Route path="/order/edit/:id" component={EditOrderPage}/>
          <Route path="/order/print/" exact>
            <Redirect to="/"/>
          </Route>
          <Route path="/order/print/:id" component={OrderPrint}/>
          <Route path="*" exact={false}>
            <Redirect to="/orders/new"/>
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
