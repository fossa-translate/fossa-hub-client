import {useDispatch} from 'react-redux';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {
  faBars, faDoorOpen, faList, faPlusSquare, faCog, faAngleDoubleLeft, faAngleDoubleRight
} from '@fortawesome/free-solid-svg-icons';
import Logo from '../images/fossalogo_mini.png';
import {logout} from '../slices/userSlice';
import {useState} from "react";

function NavLink({link, icon, text}) {
  return <Link to={link} className="nav-link">
    <FontAwesomeIcon icon={icon}/>
    <p className='nav-link-text'>{text}</p>
  </Link>
}

function Navigation() {
  const dispatch = useDispatch();
  const [minimized, setMinimized] = useState(false);
  const onLogoutClick = () => dispatch(logout());
  const minimize = () => {
    setMinimized(!minimized);
  }

  const Minimizer = () => {
    let icon = minimized ? faAngleDoubleLeft: faAngleDoubleRight
    return <FontAwesomeIcon onClick={minimize} icon={icon}/>
  }

  const className = 'nav-bar' + (minimized ? ' nav-bar-collapsed' : '');
  return (
    <>
      <div className={className}>
        <Minimizer/>
        <Link to="/" className="logo">
          <img src={Logo} alt="fossa-translate-logo"/>
          <p className="logo-text">Fossa-translate</p>
        </Link>
        <label htmlFor="nav-mobile" className="nav-mobile-handle">
          <FontAwesomeIcon icon={faBars}/>
        </label>
        <input type="checkbox" id="nav-mobile" className="nav-mobile"/>
        <div className="nav">
          <NavLink link='/orders' icon={faList} text="Заказы"/>
          <NavLink link='/order/new' icon={faPlusSquare} text="Создание заказа"/>

          <div className="bottom-nav">
            <NavLink link='/settings' icon={faCog} text="Настройки"/>
            <a className="nav-link" onClick={onLogoutClick}>
              <FontAwesomeIcon icon={faDoorOpen} fixedWidth/>
              <p className="nav-link-text">Выйти</p>
            </a>
          </div>
        </div>
      </div>
    </>
  );
}

export default Navigation;
