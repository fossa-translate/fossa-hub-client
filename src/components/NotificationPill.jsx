import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

function NotificationPill({error, onClick}) {
    if (!error) {
        return <></>
    }
    return (
        <div className="notification-pill notification-pill-error" onClick={onClick}>
            {error.message}
            <FontAwesomeIcon icon={faTimes}/>
        </div>
    )
}

export default NotificationPill;