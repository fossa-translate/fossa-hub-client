function InputCheckbox({id, title, children, ...props}) {
  const titleChild = title && <label htmlFor={id}>{title}</label>;
  return <div className="checkbox">
    <input {...props}
           type="checkbox"
           id={id}
           name={id}
    />
    {titleChild}
    {children}
  </div>;
}

export default InputCheckbox;
