function Input({
                 autoComplete = false,
                 id,
                 placeholder,
                 title,
                 type = 'text',
                 ...props
               }) {
  return <div className="input">
    {
      title && <label htmlFor={id}>{title}</label>
    }
    <input id={id}
           type={type}
           placeholder={placeholder || title}
           autoComplete={autoComplete && 'on' || 'off'}
           name={id}
           {...props}
    />
  </div>;
}

export default Input;
