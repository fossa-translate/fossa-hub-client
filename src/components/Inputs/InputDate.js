import Input from './Input';

function InputDate(props) {
  return <div className="input">
    <Input {...props}
           type="datetime-local"
           max="9999-12-31T23:59"
    />
  </div>;
}

export default InputDate;
