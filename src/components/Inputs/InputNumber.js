import Input from './Input';

function InputNumber(props) {
  return <Input {...props}
           type="number"
           min="0"
    />
}

export default InputNumber;
