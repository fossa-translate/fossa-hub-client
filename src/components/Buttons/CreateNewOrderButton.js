import {Link} from 'react-router-dom';

function CreateNewOrderBtn() {
  return <Link to='/order/new' className="button">Создать новый</Link>;
}

export default CreateNewOrderBtn;
