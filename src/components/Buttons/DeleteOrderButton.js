import {faTrash} from '@fortawesome/free-solid-svg-icons';
import OrderButton from './OrderButton';
import {deleteOrder} from '../../slices/orderSlice';
import {useDispatch} from 'react-redux';

function DeleteOrderButton({order, match}) {
  const dispatch = useDispatch();
  const onClickDelete = () => dispatch(deleteOrder({
    orderNumber: order.orderNumber,
    tabName: match.params.tabName
  }))
return <OrderButton style='negative' title="Удалить" faIcon={faTrash} onClick={onClickDelete}/>;
}

export default DeleteOrderButton;
