import {faArchive} from '@fortawesome/free-solid-svg-icons';
import OrderButton from './OrderButton';
import {useDispatch} from 'react-redux';
import {updateOrderStatus} from '../../slices/orderSlice';

function ArchiveOrderButton({order, match}) {
  const dispatch = useDispatch();
  const onClickArchive = () => dispatch(updateOrderStatus({
    orderNumber: order.orderNumber,
    status: 'archive',
    tabName: match.params.tabName,
  }));
  return <OrderButton title="В архив" faIcon={faArchive} onClick={onClickArchive}/>;
}

export default ArchiveOrderButton;
