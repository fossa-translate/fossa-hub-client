import {useHistory} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {orderSetCurrent} from '../../slices/orderSlice';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit} from '@fortawesome/free-solid-svg-icons';
import OrderButton from './OrderButton';

function OrderEditButton({order}) {
  const history = useHistory();
  const dispatch = useDispatch();

  const onClickEdit = () => {
    if (order) {
      dispatch(orderSetCurrent(order));
    }
    const path = `/order/edit/${order.orderNumber}`;
    history.push(path);
  };
  return (
    <OrderButton onClick={onClickEdit} faIcon={faEdit} title='Редактировать'/>
  );
}

export default OrderEditButton;
