import {faBackspace} from '@fortawesome/free-solid-svg-icons';
import OrderButton from './OrderButton';
import {useDispatch} from 'react-redux';
import {updateOrderStatus} from '../../slices/orderSlice';

function ReturnOrderButton({order, match}) {
  const dispatch = useDispatch();
  const onClickReturn = () => dispatch(updateOrderStatus({
    orderNumber: order.orderNumber,
    status: 'lower',
    tabName: match.params.tabName,
  }));
  return <OrderButton title="Вернуть" faIcon={faBackspace} onClick={onClickReturn}/>;
}

export default ReturnOrderButton;
