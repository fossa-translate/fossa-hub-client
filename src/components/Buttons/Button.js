function Button({children, style, ...props}) {
  let className = "button";
  if (style) {
    className += ' button-' + style;
  }
  return <button className={className} {...props}>{children}</button>;
}

export default Button;
