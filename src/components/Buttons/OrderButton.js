import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Button from './Button';

function OrderButton({title, faIcon, ...props}) {
  return <Button {...props}>
    <FontAwesomeIcon icon={faIcon} fixedWidth/>
    <div className="button-text">{title}</div>
  </Button>;
}

export default OrderButton;
