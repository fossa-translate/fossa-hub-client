import {faBriefcase} from '@fortawesome/free-solid-svg-icons';
import OrderButton from './OrderButton';
import {useDispatch} from 'react-redux';
import {updateOrderStatus} from '../../slices/orderSlice';

function ToWorkOrderButton({order, match}) {
  const dispatch = useDispatch();
  const onClickToWork = () => dispatch(updateOrderStatus({
    orderNumber: order.orderNumber,
    status: 'raise',
    tabName: match.params.tabName,
  }));
  return <OrderButton title="В работу" faIcon={faBriefcase} onClick={onClickToWork}/>;
}

export default ToWorkOrderButton;
