import {Link} from 'react-router-dom';

function PrintOrderButton({order}) {
  const to = `/order/print/${order.orderNumber}`;
  return <Link to={to} className="button">Печать</Link>;
}

export default PrintOrderButton;
