import OrderListEntry from './OrderListEntry';

function OrderList({isFetching, orders, buttons, match}) {
  if (isFetching) {
    return <div className="orders-list">Loading...</div>;
  }

  if (orders && !orders.length) {
    return <div className="orders-list">Empty list</div>;
  }

  if (orders) {
    return (
      <div className="orders-list">
        {orders.map((order) => <OrderListEntry key={order.orderNumber} order={order} buttons={buttons} match={match}/>)}
      </div>
    );
  }

  return 'error :(';
}

export default OrderList;
