import {useSelector, useDispatch} from "react-redux";
import {deleteError} from "../slices/errorsSlice";
import NotificationPill from "./NotificationPill";

function NotificationsBlock() {
    const dispatch = useDispatch();
    const dataSelector = (store) => store.errors;
    const errors = useSelector(dataSelector);

    return <div className='notifications-block'>
        {
            !Object.keys(errors).length && <></> ||
            Object.entries(errors).map(([id, error]) => {
                    const onClose = () => {
                        dispatch(deleteError(id))
                    }

                return <NotificationPill error={error} onClick={onClose}/>
                }
            )

        }
    </div>
}

export default NotificationsBlock;