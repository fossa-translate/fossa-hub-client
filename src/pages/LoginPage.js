import '../styles/Login.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Logo from '../images/fossalogo_mini.png';
import { authorize } from '../slices/userSlice';

function LoginPage() {
  const dataSelector = (store) => store.user.error;
  const error = useSelector(dataSelector);
  const errorAsString = error && error.toString();

  const dispatch = useDispatch();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const onChangeUsername = (event) => setUsername(event.target.value);
  const onChangePassword = (event) => setPassword(event.target.value);
  const onSubmitLogin = () => dispatch(authorize({ username, password }));

  return (
    <div className="login-wrapper">
      <div className="login-form-logo">
        <img src={Logo} alt="fossa-logo" />
        <p>Fossa-translate</p>
      </div>

      <div className="login-form">
        <div className="login-title">Войти</div>
        <div className="login-row">
          <div className="login-input-label">Логин</div>
          <div className="login-input-wrapper">
            <FontAwesomeIcon icon={faUser} />
            <input
              type="text"
              name="username"
              className="login-input"
              placeholder="Введите ваш логин"
              autoComplete="off"
              onChange={onChangeUsername}
            />
          </div>
        </div>

        <div className="login-row">
          <div className="login-input-label">Пароль</div>
          <div className="login-input-wrapper">
            <FontAwesomeIcon icon={faLock} />
            <input
              type="password"
              name="password"
              className="login-input"
              placeholder="Введите ваш пароль"
              onChange={onChangePassword}
            />
          </div>
        </div>
        <button className="login-submit" onClick={onSubmitLogin}>Войти</button>
        <p>{errorAsString}</p>
      </div>
    </div>
  );
}

export default LoginPage;
