import NewOrderPage from './NewOrderPage';
import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';
import {getOrder} from '../slices/orderSlice';

function EditOrderPage(props) {
  const dispatch = useDispatch();
  const dataSelector = (store) => store.order.current;
  const order = useSelector(dataSelector);

  useEffect(() => {
    if (!order) {
      const orderNumber = props.match.params.id;
      dispatch(getOrder(orderNumber));
    }
  });

  if (!order) {
    return 'Loading...';

  }
  return <NewOrderPage order={order} {...props}/>
}

export default EditOrderPage;
