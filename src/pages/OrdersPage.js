import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getOrders} from '../slices/orderSlice';
import OrderList from '../components/OrderList';
import OrderListTabs from '../components/OrderListTabs';
import tabs from '../tabs';
import {useHistory} from 'react-router-dom';

function OrdersPage({match}) {
  const [tabName, setTabName] = useState(match.params.tabName);
  const dispatch = useDispatch();
  const history = useHistory();
  const dataSelector = (store) => store.order;
  const orderStore = useSelector(dataSelector);
  const orders = orderStore.list;
  const { isFetching } = orderStore;
  useEffect(() => {
    if (!orders) {
      dispatch(getOrders(tabName));
    }
  }, [orders]);

  const onTabPanelClick = ({target}) => {
    if (tabName === target.id) {
      return;
    }
    setTabName(target.id);
    history.push(target.id);
    dispatch(getOrders(target.id));
  };

  const onSearchInput = ({target}) => {
    const filter = target.value;
    const params = {filter};
    dispatch(getOrders(tabName, params));
  };

  return (
    <div className="orders-wrapper">
      <div className="orders-top-row">
        <OrderListTabs onClick={onTabPanelClick} tabName={tabName}/>
        <div className="orders-search-wrapper">
          <label htmlFor="searchInput">Поиск: </label>
          <input id="searchInput" className="orders-search-input" type="text" autoComplete="off"
                 onInput={onSearchInput} onChange={onSearchInput}/>
        </div>
      </div>
      <OrderList orders={orders} isFetching={isFetching} buttons={tabs[tabName].buttons} match={match}/>
    </div>
  );
}

export default OrdersPage;
