import {useState, useEffect} from 'react';
import languages from '../languages.json';
import LanguagesInputRow from '../components/LanguagesInputRow';
import CreateNewOrderButton from '../components/Buttons/CreateNewOrderButton';
import PrintOrderButton from '../components/Buttons/PrintOrderButton';
import SaveOrderButton from '../components/Buttons/SaveOrderButton';
import {useDispatch} from 'react-redux';
import {saveOrder} from '../slices/orderSlice';
import Input from '../components/Inputs/Input';
import InputCheckbox from '../components/Inputs/InputCheckbox';
import InputNumber from '../components/Inputs/InputNumber';
import InputDate from '../components/Inputs/InputDate';

function refineNumber(value) {
  if (!value) {
    return 0;
  }

  const startsFromZero = value.length > 1 && value.toString()[0] === '0';
  if (startsFromZero) {
    return value.slice(1);
  }

  return value;
}

function applyPhoneMask(value) {
  value = value.replace(/[\D\s]/gm, '');
  value = value.slice(0, 11);
  let number = '+7 (';
  const formatStrings = {
    2: ') ',
    5: '-',
    7: '-',
  };
  if (value[0] === '7' || value[0] === '8') {
    value = value.slice(1);
  }
  for (let i = 0; i < value.length; i++) {
    number += value[i];
    if (formatStrings[i]) {
      number += formatStrings[i];
    }
  }
  return number;
}

const masks = {
  phone: applyPhoneMask,
};

function ButtonsRow({order}) {
  if (!order.orderNumber) {
    return (
        <div className="buttons-row">
          <SaveOrderButton/>
        </div>
    );
  }

  return (
      <div className="buttons-row">
        <CreateNewOrderButton/>
        <PrintOrderButton order={order}/>
        <SaveOrderButton/>
      </div>
  );
}

function NewOrderPage({order}) {
  if (!order) {
    order = {
      price: 0,
      bureauPrice: 0,
      notaryPrice: 0,
      prepayment: 0,
      pageCount: 1,
      notaryCheck: false,
      languageFrom: languages.defaults[0],
      languageTo: languages.defaults[1],
    };
  }

  const [price, setPrice] = useState(order.price);
  const [bureauPrice, setBureauPrice] = useState(order.bureauPrice);
  const [notaryPrice, setNotaryPrice] = useState(order.notaryPrice);
  const [pageCount, setPageCount] = useState(order.pageCount);
  const [notaryCheck, setNotaryCheck] = useState(order.notaryCheck);
  const [prepayment, setPrepayment] = useState(order.prepayment);
  const dispatch = useDispatch();

  useEffect(() => {
    calculatePrice();
  });

  const calculatePrice = () => {
    let updatedPrice = +bureauPrice * +pageCount;
    if (notaryCheck) {
      updatedPrice += +notaryPrice;
    }
    setPrice(updatedPrice);
  };

  const onChangeBureauPrice = ({target}) => setBureauPrice(refineNumber(target.value));
  const onChangeNotaryPrice = ({target}) => setNotaryPrice(refineNumber(target.value));
  const onChangeNotaryCheck = ({target}) => setNotaryCheck(target.checked);
  const onChangePageCount = ({target}) => setPageCount(refineNumber(target.value));
  const onChangePrepayment = ({target}) => setPrepayment(refineNumber(target.value));
  const onContactsChanged = (event) => {
    if (event.nativeEvent.inputType === 'deleteContentBackward') {
      return;
    }
    if (event.nativeEvent.data === ',') {
      return;
    }

    event.target.value = event.target.value
        .split(', ')
        .map((v) => {
          v.trim();
          const isPhone = v.startsWith('+') || v.startsWith('8');
          if (isPhone) {
            return masks.phone(v);
          }
          return v;
        })
        .join(', ');
  };
  const onSubmitOrder = (event) => {
    event.stopPropagation();
    event.preventDefault();
    const formData = new FormData(event.target);
    if (order.orderNumber) {
      formData.append('orderNumber', order.orderNumber);
    }
    dispatch(saveOrder(formData));
  };

  return (
      <div className="order-wrapper">
        <form className="order-form" onSubmit={onSubmitOrder}>
          <div className="form-name">
            {
              order.orderNumber
              && <h1>Изменение заказа №{order.orderNumber}</h1>
              || <h1>Создание заказа</h1>
            }

            <ButtonsRow order={order}/>
          </div>

          <div className="input-group">
            <div className="input-row">
              <InputDate id="dateFrom" required defaultValue={order.dateFrom} title="От"/>
              <InputDate id="dateTo" required defaultValue={order.dateTo} title="До"/>
            </div>
            <div className="input-row">
              <Input id="clientName" required defaultValue={order.clientName} title="ФИО клиента"/>
              <Input id="contacts" required defaultValue={order.contacts}
                     placeholder="Контактные данные" title="Контакты" onInput={onContactsChanged}/>
            </div>
            <div className="input-row">
              <Input id="listDocs" required defaultValue={order.listDocs} title="Документы"/>
              <Input id="whereyouknow" required defaultValue={order.whereyouknow}
                     title="Откуда узнали"/>
            </div>
            <Input id="additionalMaterials" required defaultValue={order.additionalMaterials}
                   title="Дополнительно"/>
          </div>
          <h1 className="input-group-title">Перевод</h1>
          <div className="input-group">
            <Input id="pageCount" type="number" required value={pageCount} title="Cтраниц"
                   onInput={onChangePageCount}/>
            <LanguagesInputRow from={order.languageFrom} to={order.languageTo}/>
          </div>
          <div className="input-columns">
            <div className="column">
              <h1 className="input-group-title">Услуги</h1>
              <div className="input-group">
                <div className="input-column">
                  <InputCheckbox id="expressCheck" defaultChecked={order.expressCheck}
                                 title="Срочно"/>
                  <InputCheckbox id="notaryCheck" checked={notaryCheck} onChange={onChangeNotaryCheck}
                                 title="Нотариальное заверение">
                    <Input id="notaryPrice" required value={notaryPrice} type="number"
                           onInput={onChangeNotaryPrice}/>
                  </InputCheckbox>
                  <InputCheckbox id="apostyleCheck" defaultChecked={order.apostyleCheck}
                                 title="Апостиль"/>
                  <InputCheckbox id="signFossa" defaultChecked={order.signFossa} title="Печать бюро"/>
                </div>
              </div>
            </div>
            <div className="column">
              <h1 className="input-group-title">Цена услуг</h1>
              <div className="input-group">
                <div className="input-column">
                  <InputNumber id="bureauPrice" required value={bureauPrice}
                               onInput={onChangeBureauPrice} title="Бюро"/>
                  <InputNumber id="prepayment" required value={prepayment}
                               onInput={onChangePrepayment} title="Предоплата"/>
                </div>
                <div className="order-price-sum">
                  <p>Итого:</p>
                  <div className="order-price">{price}</div>
                  <div className="currency">₽уб.</div>
                </div>
              </div>
            </div>
          </div>
          <ButtonsRow order={order}/>
        </form>
      </div>
  );
}

export default NewOrderPage;
