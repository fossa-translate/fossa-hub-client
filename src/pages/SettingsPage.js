import Input from '../components/Inputs/Input';
import Button from '../components/Buttons/Button';

function SettingsPage() {
  return <>
    <div className="row">
      <div className="column">
        <div className="input-group">
          <Input title="Новый пароль"/>
          <Input title="Повторить пароль"/>
        </div>
      </div>
    </div>
    <div className="column">

    </div>
    <div className="buttons-row">
      <Button style="positive">Сохранить</Button>
    </div>
  </>;
}

export default SettingsPage;
