import axios from 'axios';
import qs from 'qs';

const apiServerUrl = `http://${location.hostname}:4221`;

export default function makeRequest(apiPath, options = {}) {
  let url = apiServerUrl + apiPath;
  if (options.query) {
    url += '?' + qs.stringify(options.query);
  }

  return axios(url, options);
}

export function postRequest(apiPath, options) {
  return makeRequest(apiPath, {
    method: 'POST',
    ...options,
  });
}

export function getRequest(apiPath, options) {
  return makeRequest(apiPath, options);
}
