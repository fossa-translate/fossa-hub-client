import PreviewOrderButton from './components/Buttons/PreviewOrderButton';
import EditOrderButton from './components/Buttons/EditOrderButton';
import ToWorkOrderButton from './components/Buttons/ToWorkOrderButton';
import DeleteOrderButton from './components/Buttons/DeleteOrderButton';
import ArchiveOrderButton from './components/Buttons/ArchiveOrderButton';
import ReturnOrderButton from './components/Buttons/ReturnOrderButton';

const tabs = {
  'new': {
    title: 'Новые заказы',
    buttons: [PreviewOrderButton, EditOrderButton, ToWorkOrderButton, DeleteOrderButton],
  },
  'await': {
    title: 'Ожидают',
    buttons: [PreviewOrderButton, ReturnOrderButton, ArchiveOrderButton],
  },
  'in-work': {
    title: 'В работе',
    buttons: [PreviewOrderButton, ReturnOrderButton, ArchiveOrderButton],
  },
  'on-edit': {
    title: 'На редактуре',
    buttons: [PreviewOrderButton, ArchiveOrderButton],
  },
  'additional': {
    title: 'Дополнительно',
    buttons: [PreviewOrderButton, DeleteOrderButton],
  },
  'done': {
    title: 'Готовые',
    buttons: [PreviewOrderButton, ReturnOrderButton, ArchiveOrderButton],
  },
  'archive': {
    title: 'Архив',
    buttons: [PreviewOrderButton, DeleteOrderButton],
  },
};

export default tabs;
