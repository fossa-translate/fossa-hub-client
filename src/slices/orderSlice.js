import {createSlice} from '@reduxjs/toolkit';
import {getRequest, postRequest} from '../modules/request';
import {putError} from "./errorsSlice";
import {formDataFromObject} from "../modules/utils";

function deleteFromArray(array, condition) {
  for (let i = 0; i < array.length; i++) {
    const val = array[i];
    if (condition(val)) {
      array.splice(i, 1);
      return true;
    }
  }
  return false;
}

function deleteOrderFromStore(array, orderNumber) {
  deleteFromArray(array, (obj) => orderNumber === parseInt(obj.orderNumber));
  return array;
}

const initialState = {
  isFetching: false,
  list: null,
  message: null,
  current: null,
};

const orderSlice = createSlice({
  name: 'order',
  initialState,
  reducers: {
    ordersFetching(state) {
      state.isFetching = true;
    },
    ordersFetched(state, {payload}) {
      state.isFetching = false;
      state.list = payload;
    },
    orderSaveFetching(state) {
      state.isFetching = true;
    },
    orderSaveFetched(state) {
      state.isFetching = false;
      state.message = 'Successfully saved';
    },
    orderSetCurrent(state, {payload}) {
      state.current = payload;
    },
    updateCurrentOrder(state, {payload}) {
      state.current = {
        ...state.current,
        ...payload
      };
    },
    orderGetFetching(state) {
      state.isFetching = false;
    },
    orderGetFetched(state) {
      state.isFetching = true;
    },
    deleting(state) {
      state.isFetching = true;
    },
    deletingSuccessful(state, {payload}) {
      state.isFetching = false;
      state.list = deleteOrderFromStore(state.list, payload);
    },
    updatingStatus(state) {
      state.isFetching = false;
    },
    updatingStatusSuccessful(state) {
      state.isFetching = true;
    }
  },
});

export const {actions, reducer} = orderSlice;
export const {
  updateCurrentOrder,
  ordersFetching,
  ordersFetched,
  orderSaveFetching,
  orderSaveFetched,
  orderGetFetching,
  orderGetFetched,
  orderSetCurrent,
  deleting,
  deletingSuccessful,
  updatingStatus,
  updatingStatusSuccessful,
} = actions;
export default reducer;

const apiGetOrdersPath = '/api/orders/get/';
export const getOrders = (orderType, filterQuery) => async (dispatch) => {
  dispatch(ordersFetching());
  try {
    const {data} = await getRequest(apiGetOrdersPath + orderType, {query: filterQuery});
    dispatch(ordersFetched(data.orders));
  } catch (error) {
    dispatch(putError(error));
  }
};

const saveOrderPath = '/api/order/save';
export const saveOrder = (order) => async (dispatch) => {
  // const data = formDataFromObject(order);
  dispatch(orderSaveFetching());
  try {
    await postRequest(saveOrderPath, {data: order});
    dispatch(orderSaveFetched());
  } catch (error) {
    dispatch(putError(error));
  }
};

const getOrderPath = '/api/order/get/';
export const getOrder = (orderNumber) => async (dispatch) => {
  dispatch(orderGetFetching());
  try {
    const {data} = await getRequest(getOrderPath + orderNumber);
    dispatch(orderSetCurrent(data.order));
    dispatch(orderGetFetched());
  } catch (error) {
    dispatch(putError(error));
  }
};

const deleteOrderPath = '/api/order/delete/';
export const deleteOrder = ({orderNumber, tabName}) => async (dispatch) => {
  dispatch(deleting());
  try {
    await postRequest(deleteOrderPath + orderNumber);
    dispatch(deletingSuccessful(orderNumber));
    dispatch(getOrders(tabName));
  } catch (error) {
    dispatch(putError(error));
  }
};

const updateOrderStatusPath = '/api/order/edit/status/';
export const updateOrderStatus = ({orderNumber, status, tabName}) => async (dispatch) => {
  dispatch(updatingStatus());
  try {
    await postRequest(updateOrderStatusPath + orderNumber + '/' + status);
    dispatch(updatingStatusSuccessful());
    dispatch(getOrders(tabName));
  } catch (error) {
    dispatch(putError(error));
  }
};
