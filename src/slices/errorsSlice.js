import {createSlice} from "@reduxjs/toolkit";

const initialState = {};

function generateId () {
    return Math.random().toString(36).substring(7);
}

const errorSlice = createSlice({
    name: 'errors',
    initialState,
    reducers: {
        putError(state, {payload}) {
            let id = generateId();
            if (state[id]) {
                console.warn('Error id collision ?');
                id = generateId();
            }
            state[id] = payload;
        },
        deleteError(state, {payload}) {
            delete state[payload];
        }
    }
})

export const { actions, reducer } = errorSlice;
export const {
    putError,
    deleteError,
} = actions;
export default reducer;