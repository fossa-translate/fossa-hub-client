import { createSlice } from '@reduxjs/toolkit';
import { postRequest } from '../modules/request';
import {putError} from "./errorsSlice";

export const saveProfile = (profile) => {
  localStorage.setItem('profile', JSON.stringify(profile));
};

export const getProfile = () => JSON.parse(localStorage.getItem('profile'));

export const removeProfile = () => {
  localStorage.removeItem('profile');
};

const initialState = {
  isFetching: false,
  profile: getProfile(),
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    authFetching(state) {
      state.isFetching = true;
    },
    authFetched(state, { payload }) {
      state.isFetching = false;
      state.profile = payload;
    },
    authLogout(state) {
      state.profile = null;
    },
  },
});

export const { actions, reducer } = userSlice;
export const {
  authFetching,
  authFetched,
  authLogout,
} = actions;
export default reducer;

const apiLoginPath = '/login';
export const authorize = ({ username, password }) => async (dispatch) => {
  dispatch(authFetching());
  try {
    const options = { data: { username, password } };
    const { data } = await postRequest(apiLoginPath, options);
    saveProfile(data);
    dispatch(authFetched(data));
  } catch (error) {
    dispatch(putError(error));
  }
};

export const logout = () => (dispatch) => {
  removeProfile();
  dispatch(authLogout());
};
