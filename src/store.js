import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import user from './slices/userSlice';
import order from './slices/orderSlice';
import errors from './slices/errorsSlice';

const middleware = getDefaultMiddleware({
  immutableCheck: false,
  serializableCheck: false,
  thunk: true,
});

const reducer = combineReducers({
  user,
  order,
  errors
});

const devTools = process.env.NODE_ENV !== 'production';

const store = configureStore({
  reducer,
  middleware,
  devTools,
});
export default store;
